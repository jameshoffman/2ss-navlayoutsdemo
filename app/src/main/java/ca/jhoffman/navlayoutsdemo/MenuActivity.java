package ca.jhoffman.navlayoutsdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

    // https://developer.android.com/guide/topics/ui/menus#xml

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    // On configure les items du menu dans menu/options.xml
    // On peut personnaliser l'affichage des items du menu dans ce fichier avec l'attribut showAsAction="..."
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Comme la bottom nav, on peut reagir a l'item sélectionné
        /*
        switch (item.getItemId()) {
            R.id.menuOne: ...
            break;

            R.id.menuTwo: ...
            break;

            default:
                return super.onOptionsItemSelected(item);
        }
        */

        if (item.getGroupId() == R.id.menuOptions) {
            // On encapsule les options de notre menu dans un groupe pour les identifier facilement
            Toast.makeText(this, "Selected: "  + item.getTitle(), Toast.LENGTH_SHORT).show();
            return true;
        } else {
            // Important de déléguer le travail si ce n'est pas une option de NOTRE menu
            // par exemple, le Up button pour la navigation
            return super.onOptionsItemSelected(item);
        }
    }
}