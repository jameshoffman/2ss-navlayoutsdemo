package ca.jhoffman.navlayoutsdemo.bottom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ca.jhoffman.navlayoutsdemo.R;

public class ABottomActivity extends BottomActivity {

    @Override
    int getMenuItemId() {
        return R.id.bottomA;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_a_bottom);
        // On assigne la vue avant d'appeler super, car la classe parent initialise la bottom nav
        super.onCreate(savedInstanceState);
    }
}