package ca.jhoffman.navlayoutsdemo.bottom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ca.jhoffman.navlayoutsdemo.R;

public class BBottomActivity extends BottomActivity {

    @Override
    int getMenuItemId() {
        return R.id.bottomB;
        // Chaque sous-classe est responsable de fournir le id de l'item associé
        // permet de sélectionner le bon au démarrage
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_b_bottom);
        // Voir le fichier de layout pour constater le include de layout

        super.onCreate(savedInstanceState);
    }
}