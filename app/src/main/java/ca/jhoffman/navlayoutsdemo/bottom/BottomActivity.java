package ca.jhoffman.navlayoutsdemo.bottom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import ca.jhoffman.navlayoutsdemo.R;

public abstract class BottomActivity extends AppCompatActivity {
    // Une classe abstraite qui permets de reutiliser la logique de navigation
    // lorsque qu'un item est selectionné

    private BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // IMPORTANT de ne pas setContentView(), ce sont les sous-classes qui s'en occupent
        // Voir le layout partial_bottom qui est réutilisé dans les différentes activities
        // on y configure également le menu via la ressource menu/bottom_nav.xml

        navigationView = (BottomNavigationView)findViewById(R.id.bottomNav);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            // Listerer de sélection d'un item
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Class destinationActivity = null;

                // Selon l'item de menu sélectionné, on choisi la bonne activity
                switch (menuItem.getItemId()) {
                    case R.id.bottomA:
                        destinationActivity = ABottomActivity.class;
                        break;

                    case R.id.bottomB:
                        destinationActivity = BBottomActivity.class;
                        break;

                    case R.id.bottomC:
                        destinationActivity = CBottomActivity.class;
                        break;
                }

                Intent intent = new Intent(BottomActivity.this, destinationActivity);
                startActivity(intent);
                overridePendingTransition(0, 0);
                // l'intent est démarré en annulant la transition pour simuler qu'on ne change pas d'écran

                finish();
                // On finish pour que toutes les activities utilisable via la bottom nav ait l'air d'etre une seule
                // l'activity choisi par la bottom nav est toujours la derniere sur la pile de navigation

                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        navigationView.getMenu().findItem(getMenuItemId()).setChecked(true);
        // Lorsque l'activity démarre on sélectionne le bon item dans le menu
    }

    abstract int getMenuItemId();
}