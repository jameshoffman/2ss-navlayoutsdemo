package ca.jhoffman.navlayoutsdemo.bottom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ca.jhoffman.navlayoutsdemo.R;

public class CBottomActivity extends BottomActivity {

    @Override
    int getMenuItemId() {
        return R.id.bottomC;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_c_bottom);

        super.onCreate(savedInstanceState);

    }
}