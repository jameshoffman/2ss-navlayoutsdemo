package ca.jhoffman.navlayoutsdemo.tabs;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ca.jhoffman.navlayoutsdemo.R;

public class FirstTabActivity extends TabsActivity {

    @Override
    public int getPosition() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_first_tab);

        super.onCreate(savedInstanceState);
    }
}