package ca.jhoffman.navlayoutsdemo.tabs;

import android.os.Bundle;

import ca.jhoffman.navlayoutsdemo.R;

public class SecondTabActivity extends TabsActivity {

    @Override
    public int getPosition() {
        return 1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_second_tab);

        super.onCreate(savedInstanceState);
    }
}