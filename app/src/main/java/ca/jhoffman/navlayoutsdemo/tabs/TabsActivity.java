package ca.jhoffman.navlayoutsdemo.tabs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

import ca.jhoffman.navlayoutsdemo.R;
import ca.jhoffman.navlayoutsdemo.bottom.BottomActivity;

public abstract class TabsActivity extends AppCompatActivity {

    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
View v = findViewById(R.id.tabsNav);
        tabLayout = (TabLayout)findViewById(R.id.tabsNav);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() != getPosition()) {
                    // Appliquer seulement si la selection est differente, sinon dans onStart on crée une boucle infinie

                    Class[] activities = { FirstTabActivity.class, SecondTabActivity.class, ThirdTabActivity.class };

                    Intent intent = new Intent(TabsActivity.this, activities[tab.getPosition()]);
                    startActivity(intent);
                    overridePendingTransition(0, 0);

                    finish();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {  }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {  }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        tabLayout.getTabAt(getPosition()).select();
    }

    public abstract int getPosition();
}