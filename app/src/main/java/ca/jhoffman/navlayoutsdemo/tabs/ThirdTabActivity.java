package ca.jhoffman.navlayoutsdemo.tabs;

import android.os.Bundle;

import ca.jhoffman.navlayoutsdemo.R;

public class ThirdTabActivity extends TabsActivity {

    @Override
    public int getPosition() {
        return 2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_third_tab);

        super.onCreate(savedInstanceState);
    }
}